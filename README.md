# Come with me Dialog

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/trac/pepper)

This is a dialog application, in both Czech and English, that enables the robot to come with a person hand in hand.

Start with "Pepper, come with me" / "Karle, pojď se mnou".

See the dialog in action at [this video](https://nlp.fi.muni.cz/trac/pepper/wiki/NlpPepperShows#ComewithmedialogDialogPoj%C4%8Fsemnou).

## Installation

* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the package as usual for the Pepper robot
* after that change ENABLE_DEACTIVATION_OF_FALL_MANAGER to true in the [Fall Manager](http://doc.aldebaran.com/2-5/naoqi/motion/reflexes-fall-manager.html#how-to-disable-this-reflex) to allow walking very close to the robot. This will not turn the protection off, it just allows to use the [ALMotion::setExternalCollisionProtectionEnabled](http://doc.aldebaran.com/2-5/naoqi/motion/reflexes-external-collision-api.html#ALMotionProxy::setExternalCollisionProtectionEnabled__ssCR.bCR) function.

