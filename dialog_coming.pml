<?xml version="1.0" encoding="UTF-8" ?>
<Package name="dialog_coming" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions/>
    <Dialogs>
        <Dialog name="dlg_coming" src="dlg_coming/dlg_coming.dlg" />
    </Dialogs>
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="dialog_coming_service" src="scripts/dialog_coming_service.py" />
        <File name="__init__" src="scripts/stk/__init__.py" />
        <File name="events" src="scripts/stk/events.py" />
        <File name="logging" src="scripts/stk/logging.py" />
        <File name="runner" src="scripts/stk/runner.py" />
        <File name="services" src="scripts/stk/services.py" />
    </Resources>
    <Topics>
        <Topic name="dlg_coming_czc" src="dlg_coming/dlg_coming_czc.top" topicName="dlg_coming" language="cs_CZ" />
        <Topic name="dlg_coming_enu" src="dlg_coming/dlg_coming_enu.top" topicName="dlg_coming" language="en_US" />
    </Topics>
    <IgnoredPaths>
        <Path src=".metadata" />
    </IgnoredPaths>
</Package>
